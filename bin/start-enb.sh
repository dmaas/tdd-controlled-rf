#!/bin/bash

set -eux
cd /local/openairinterface5g/ || exit
source oaienv
cd /local/openairinterface5g/cmake_targets || exit
sudo -E ./lte_build_oai/build/lte-softmodem -O /usr/local/etc/oai/enb.conf --eNBs.[0].rrc_inactivity_threshold 0
