#!/bin/bash

# Setup OAI eNB

set -eux
echo "Starting setup for eNB..."
VLANIF=$(ifconfig | grep vlan | cut -d ":" -f1)
VLANIP=$(ip addr list "$VLANIF" | grep "inet " | cut -d' ' -f6|cut -d/ -f1)
EPCIP=$(getent hosts epc | awk '{ print $1 }')
PREFIX="/usr/local/etc/oai"

while ! wget -qO - http://repos.emulab.net/emulab.key | sudo apt-key add -
do
    echo Failed to get emulab key, retrying
done

while ! sudo add-apt-repository -y http://repos.emulab.net/powder-testing/ubuntu/
do
    echo Failed to get johnsond ppa, retrying
done

while ! sudo apt-get update
do
    echo Failed to update, retrying
done

while ! sudo DEBIAN_FRONTEND=noninteractive apt-get install -y libuhd-dev linux-tools-$(uname -r)
do
    echo Failed to get gnuradio, retrying
done

while ! sudo "/usr/lib/uhd/utils/uhd_images_downloader.py"
do
    echo Failed to download uhd images, retrying
done

cd /local || exit
while ! git clone https://gitlab.eurecom.fr/oai/openairinterface5g.git
do
    echo Failed to clone openairinterface5g, retrying
done

# in order for config to persist after reboot
sudo mkdir -m 0777 -p $PREFIX
sudo chmod 777 $PREFIX
cp /local/repository/etc/oai-enb.conf $PREFIX/enb.conf
sed -i -e "s/CI_MME_IP_ADDR/$EPCIP/" $PREFIX/enb.conf
sed -i -e "s/CI_ENB_IP_ADDR/$VLANIP/" $PREFIX/enb.conf
sed -i -e "s/VLANIF/$VLANIF/" $PREFIX/enb.conf

cd /local/openairinterface5g/ || exit
git checkout v1.2.0
source oaienv
cd /local/openairinterface5g/cmake_targets || exit
./build_oai -I -w USRP --eNB
echo "eNB setup complete!"
