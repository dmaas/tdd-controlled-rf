#!/bin/bash

# Setup the an all-in-one epc using openair-cn and openair-cn-cups
# Following this https://github.com/OPENAIRINTERFACE/openair-cn/wiki/Full-Deployment-of-OAI-system

set -eux
echo "Starting setup for EPC..."
VLANIF=$(ifconfig | grep vlan | head -1 | cut -d ":" -f1)
VLANIP=$(ip addr list "$VLANIF" | grep "inet " | head -1 | cut -d' ' -f6|cut -d/ -f1)
Cassandra_Server_IP="127.0.0.1"
MY_REALM="powderwireless.net"
LTE_KEY="8baf473f2f8fd09487cccbd7097c6862"
OPERATOR_KEY="11111111111111111111111111111111"
APN="oai.ipv4"
INITIAL_IMSI="999990000000002"
INITIAL_MSISDN="001011234561000"
TEST_MCC="999"
TEST_MNC="99"
INSTANCE=1
PREFIX="/usr/local/etc/oai"

while ! sudo apt-get update
do
    echo Failed to update, retrying
done

while ! sudo DEBIAN_FRONTEND=noninteractive apt-get install -y cmake
do
    echo Failed to install required packages; retrying
done

cd /local/ || exit
while ! git clone https://github.com/OPENAIRINTERFACE/openair-cn.git
do
    echo Failed to clone openair-cn, retrying
done

while ! git clone https://github.com/OPENAIRINTERFACE/openair-cn-cups.git
do
    echo Failed to clone openair-cn-cups, retrying
done

while ! git clone https://github.com/fmtlib/fmt.git
do
    echo Failed to clone fmt, retrying
done

cd /local/fmt || exit
cmake CMakeLists.txt
make
sudo make install

cd /local/openair-cn/ || exit
git checkout 18df8ed9079e1fd7c110a5783148292f0bf2398f
cp /local/repository/etc/build_helper.cassandraFIX /local/openair-cn/build/tools/build_helper.cassandra

cd /local/openair-cn/scripts/ || exit
sudo apt-key adv --keyserver pool.sks-keyservers.net --recv-key A278B781FE4B2BDA
./build_cassandra --check-installed-software --force

sudo service cassandra stop
sudo update-alternatives --config java
sudo rm -rf /var/lib/cassandra/data/system/*
sudo rm -rf /var/lib/cassandra/commitlog/*
sudo rm -rf /var/lib/cassandra/data/system_traces/*
sudo rm -rf /var/lib/cassandra/saved_caches/*
sudo cp /local/repository/etc/cassandra.yaml /etc/cassandra/cassandra.yaml
sudo service cassandra start

./build_hss_rel14 --check-installed-software --force
./build_hss_rel14 --clean
./build_mme --check-installed-software --force
./build_mme --clean

cd /local/openair-cn-cups/ || exit
git checkout v1.0.0

cd ./build/scripts || exit
./build_spgwu -I -f
./build_spgwu -c -V -b Debug -j

./build_spgwc -I -f
./build_spgwc -c -V -b Debug -j

cd /local/openair-cn/scripts/ || exit

cqlsh --file ../src/hss_rel14/db/oai_db.cql $Cassandra_Server_IP
./data_provisioning_users --apn $APN \
    --apn2 internet \
    --key $LTE_KEY \
    --imsi-first $INITIAL_IMSI \
    --msisdn-first $INITIAL_MSISDN \
    --mme-identity mme.$MY_REALM \
    --no-of-users 40 \
    --realm $MY_REALM \
    --truncate True \
    --verbose True \
    --cassandra-cluster $Cassandra_Server_IP
./data_provisioning_mme --id 3 \
    --mme-identity mme.$MY_REALM \
    --realm $MY_REALM \
    --ue-reachability 1 \
    --truncate True \
    --verbose True \
    -C $Cassandra_Server_IP

openssl rand -out $HOME/.rnd 128

cd /local/openair-cn/scripts || exit

sudo mkdir -m 0777 -p $PREFIX
sudo chmod 777 $PREFIX
sudo mkdir -m 0777 -p $PREFIX/freeDiameter
sudo mkdir -m 0777 -p $PREFIX/logs
sudo mkdir -m 0777 -p logs
cp ../etc/acl.conf ../etc/hss_rel14_fd.conf $PREFIX/freeDiameter
cp ../etc/hss_rel14.conf ../etc/hss_rel14.json $PREFIX
cp ../etc/oss.json $PREFIX

declare -A HSS_CONF
HSS_CONF[@PREFIX@]=$PREFIX
HSS_CONF[@REALM@]=$MY_REALM
HSS_CONF[@HSS_FQDN@]="hss.${HSS_CONF[@REALM@]}"
HSS_CONF[@cassandra_Server_IP@]=$Cassandra_Server_IP
HSS_CONF[@OP_KEY@]=$OPERATOR_KEY
HSS_CONF[@ROAMING_ALLOWED@]="true"
for K in "${!HSS_CONF[@]}"; do    egrep -lRZ "$K" $PREFIX | xargs -0 -l sed -i -e "s|$K|${HSS_CONF[$K]}|g"; done
sed -i -e 's/#ListenOn/ListenOn/g' $PREFIX/freeDiameter/hss_rel14_fd.conf
../src/hss_rel14/bin/make_certs.sh hss ${HSS_CONF[@REALM@]} $PREFIX
pushd $PREFIX
sudo oai_hss -j $PREFIX/hss_rel14.json --onlyloadkey
popd


cp ../etc/mme_fd.sprint.conf  $PREFIX/freeDiameter/mme_fd.conf
cp ../etc/mme.conf  $PREFIX
declare -A MME_CONF

MME_CONF[@MME_S6A_IP_ADDR@]="127.0.0.11"
MME_CONF[@INSTANCE@]=$INSTANCE
MME_CONF[@PREFIX@]=$PREFIX
MME_CONF[@REALM@]=$MY_REALM
MME_CONF[@PID_DIRECTORY@]="/var/run"
MME_CONF[@MME_FQDN@]="mme.${MME_CONF[@REALM@]}"
MME_CONF[@HSS_HOSTNAME@]="hss"
MME_CONF[@HSS_FQDN@]="${MME_CONF[@HSS_HOSTNAME@]}.${MME_CONF[@REALM@]}"
MME_CONF[@HSS_IP_ADDR@]=$Cassandra_Server_IP

# HERE I HAVE THE CORRECT MCC / NNC
MME_CONF[@MCC@]=$TEST_MCC
MME_CONF[@MNC@]=$TEST_MNC

MME_CONF[@MME_GID@]="32768"
MME_CONF[@MME_CODE@]="3"
MME_CONF[@TAC_0@]="600"
MME_CONF[@TAC_1@]="601"
MME_CONF[@TAC_2@]="602"

# ALL SUB NETWORK INTERFACES ARE $VLANIF BASED
# S1 will be the 1st interface to be reached by an eNB --> on $VLANIF and $VLANIP is the IP address on $VLANIF
MME_CONF[@MME_INTERFACE_NAME_FOR_S1_MME@]="$VLANIF"
MME_CONF[@MME_IPV4_ADDRESS_FOR_S1_MME@]="$VLANIP/24"
MME_CONF[@MME_INTERFACE_NAME_FOR_S11@]="$VLANIF:m11"
MME_CONF[@MME_IPV4_ADDRESS_FOR_S11@]="172.16.1.102/24"
MME_CONF[@MME_INTERFACE_NAME_FOR_S10@]="$VLANIF:m10"
MME_CONF[@MME_IPV4_ADDRESS_FOR_S10@]="192.168.10.110/24"
MME_CONF[@OUTPUT@]="CONSOLE"
MME_CONF[@SGW_IPV4_ADDRESS_FOR_S11_TEST_0@]="172.16.1.104/24"
MME_CONF[@SGW_IPV4_ADDRESS_FOR_S11_0@]="172.16.1.104/24"
MME_CONF[@PEER_MME_IPV4_ADDRESS_FOR_S10_0@]="0.0.0.0/24"
MME_CONF[@PEER_MME_IPV4_ADDRESS_FOR_S10_1@]="0.0.0.0/24"
# the rest is the same in Lionel setup
TAC_SGW_TEST="7"
tmph=`echo "$TAC_SGW_TEST / 256" | bc`
tmpl=`echo "$TAC_SGW_TEST % 256" | bc`
MME_CONF[@TAC-LB_SGW_TEST_0@]=`printf "%02x\n" $tmpl`
MME_CONF[@TAC-HB_SGW_TEST_0@]=`printf "%02x\n" $tmph`
MME_CONF[@MCC_SGW_0@]=${MME_CONF[@MCC@]}
MME_CONF[@MNC3_SGW_0@]=`printf "%03d\n" $(echo ${MME_CONF[@MNC@]} | sed 's/^0*//')`
TAC_SGW_0="600"
tmph=`echo "$TAC_SGW_0 / 256" | bc`
tmpl=`echo "$TAC_SGW_0 % 256" | bc`
MME_CONF[@TAC-LB_SGW_0@]=`printf "%02x\n" $tmpl`
MME_CONF[@TAC-HB_SGW_0@]=`printf "%02x\n" $tmph`
MME_CONF[@MCC_MME_0@]=${MME_CONF[@MCC@]}
MME_CONF[@MNC3_MME_0@]=`printf "%03d\n" $(echo ${MME_CONF[@MNC@]} | sed 's/^0*//')`
TAC_MME_0="601"
tmph=`echo "$TAC_MME_0 / 256" | bc`
tmpl=`echo "$TAC_MME_0 % 256" | bc`
MME_CONF[@TAC-LB_MME_0@]=`printf "%02x\n" $tmpl`
MME_CONF[@TAC-HB_MME_0@]=`printf "%02x\n" $tmph`
MME_CONF[@MCC_MME_1@]=${MME_CONF[@MCC@]}
MME_CONF[@MNC3_MME_1@]=`printf "%03d\n" $(echo ${MME_CONF[@MNC@]} | sed 's/^0*//')`
TAC_MME_1="602"
tmph=`echo "$TAC_MME_1 / 256" | bc`
tmpl=`echo "$TAC_MME_1 % 256" | bc`
MME_CONF[@TAC-LB_MME_1@]=`printf "%02x\n" $tmpl`
MME_CONF[@TAC-HB_MME_1@]=`printf "%02x\n" $tmph`
for K in "${!MME_CONF[@]}"; do    egrep -lRZ "$K" $PREFIX | xargs -0 -l sed -i -e "s|$K|${MME_CONF[$K]}|g";   ret=$?;[[ ret -ne 0 ]] && echo "Tried to replace $K with ${MME_CONF[$K]}"; done
sudo ./check_mme_s6a_certificate $PREFIX/freeDiameter mme.${MME_CONF[@REALM@]}

# SPGW-U
cd /local/openair-cn-cups/build/scripts || exit
INSTANCE=1
cp /local/repository/etc/spgw_u.conf  $PREFIX/

declare -A SPGWU_CONF
SPGWU_CONF[@INSTANCE@]=$INSTANCE
SPGWU_CONF[@PID_DIRECTORY@]="/var/run"
SPGWU_CONF[@SGW_INTERFACE_NAME_FOR_S1U_S12_S4_UP@]="$VLANIF"
SPGWU_CONF[@SGW_INTERFACE_NAME_FOR_SX@]="$VLANIF:sxu"
SPGWU_CONF[@SGW_INTERFACE_NAME_FOR_SGI@]="$VLANIF"

for K in "${!SPGWU_CONF[@]}"; do
    egrep -lZ "$K" $PREFIX/spgw_u.conf | xargs -0 -l sed -i -e "s|$K|${SPGWU_CONF[$K]}|g"; ret=$?;[[ ret -ne 0 ]] && echo "Tried to replace $K with ${SPGWU_CONF[$K]}";
done

# SPGW-C
cd /local/openair-cn-cups/build/scripts || exit
cp /local/repository/etc/spgw_c.conf  $PREFIX/

declare -A SPGWC_CONF

SPGWC_CONF[@INSTANCE@]=$INSTANCE
SPGWC_CONF[@PID_DIRECTORY@]="/var/run"
SPGWC_CONF[@SGW_INTERFACE_NAME_FOR_S11@]="$VLANIF:s11"
SPGWC_CONF[@SGW_INTERFACE_NAME_FOR_S5_S8_CP@]="$VLANIF:s5c"
SPGWC_CONF[@PGW_INTERFACE_NAME_FOR_S5_S8_CP@]="$VLANIF:p5c"
SPGWC_CONF[@PGW_INTERFACE_NAME_FOR_SX@]="$VLANIF:sxc"
SPGWC_CONF[@DEFAULT_DNS_IPV4_ADDRESS@]="193.51.196.138"
SPGWC_CONF[@DEFAULT_DNS_SEC_IPV4_ADDRESS@]="138.96.0.210"

for K in "${!SPGWC_CONF[@]}"; do
    egrep -lZ "$K" $PREFIX/spgw_c.conf | xargs -0 -l sed -i -e "s|$K|${SPGWC_CONF[$K]}|g"; ret=$?;[[ ret -ne 0 ]] && echo "Tried to replace $K with ${SPGWC_CONF[$K]}";
done

echo '200 lte' | sudo tee --append /etc/iproute2/rt_tables
echo "EPC setup complete!"
