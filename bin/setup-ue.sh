#!/bin/bash

# setup srsLTE UE

set -eux
echo "Starting setup for UE..."
while ! wget -qO - http://repos.emulab.net/emulab.key | sudo apt-key add -
do
    echo Failed to get emulab key, retrying
done

while ! sudo add-apt-repository -y http://repos.emulab.net/powder-testing/ubuntu/
do
    echo Failed to get johnsond ppa, retrying
done

while ! sudo apt-get update
do
    echo Failed to update, retrying
done

while ! sudo DEBIAN_FRONTEND=noninteractive apt-get install -y srslte srsgui uhd-host linux-tools-$(uname -r)
do
    echo Failed to get srsLTE, retrying
done

while ! sudo "/usr/lib/uhd/utils/uhd_images_downloader.py"
do
    echo Failed to download uhd images, retrying
done

sudo srslte_install_configs.sh service
echo "UE setup complete!"
