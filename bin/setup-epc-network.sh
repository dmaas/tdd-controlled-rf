#!/usr/bin/env bash

# Setup vlans and routes for epc

set -eux
VLANIF=$(ifconfig | grep vlan | cut -d ":" -f1)
VLANIP=$(ip addr list "$VLANIF" | grep "inet " | cut -d' ' -f6|cut -d/ -f1)
VLANGW=$(echo "$VLANIP" | awk -F. '{sub($NF,1);print}')

sudo ifconfig $VLANIF:m11 172.16.1.102 up
sudo ifconfig $VLANIF:m10 192.168.10.110 up
sudo ifconfig $VLANIF:sxu 172.55.55.102 up
sudo ifconfig $VLANIF:sxc 172.55.55.101 up
sudo ifconfig $VLANIF:s5c 172.58.58.102 up
sudo ifconfig $VLANIF:p5c 172.58.58.101 up
sudo ifconfig $VLANIF:s11 172.16.1.104 up

sudo ip r add default via $VLANGW dev $VLANIF table lte
sudo ip rule add from 12.0.0.0/8 table lte
sudo ip rule add from 12.1.1.0/8 table lte
